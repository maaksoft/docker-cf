FROM alpine

RUN apk --no-cache add curl ca-certificates && \
    curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar zx && \
    mv cf /usr/local/bin && \
    apk del curl && \
    cf --version
